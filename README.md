### NS2 Hive PHP
This is a small PHP wrapper for interacting with the Natural Selection 2 "Hive".

### Usage
```
$hive = new \apathy\Hive\Hive($steamId);
$hive->player->getSkill('marine');
$hive->player->getCommSkill('alien');
$hive->player->getPlaytime();
```
