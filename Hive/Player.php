<?php 

/**
 *  ▄▄▄·  ▄▄▄· ▄▄▄· ▄▄▄▄▄ ▄ .▄ ▄· ▄▌
 * ▐█ ▀█ ▐█ ▄█▐█ ▀█ •██  ██▪▐█▐█▪██▌
 * ▄█▀▀█  ██▀·▄█▀▀█  ▐█.▪██▀▐█▐█▌▐█▪
 * ▐█ ▪▐▌▐█▪·•▐█ ▪▐▌ ▐█▌·██▌▐▀ ▐█▀·.
 *  ▀  ▀ .▀    ▀  ▀  ▀▀▀ ▀▀▀ ·  ▀ •
 *  <https://fortreeforums.xyz/>
 *  Licensed under GPL-3.0-or-later 2021
 *
 *  This file is part of NS2 Hive PHP ("Hive").
 *
 *  Hive is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  Hive is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with Hive.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace apathy\Hive;

class Player
{
    protected $player;

    public function __construct($player)
    {
        $this->player = $player;
    }

    public function getPlayerId(): int
    {
        return $this->player['pid'];
    }

    public function getAlias(): string
    {
        return $this->player['string'];
    }

    public function getScore(): int
    {
        return $this->player['score'];
    }

    /**
     * Level is used for determining whether a 
     * player should move from the starting Rookie
     * tier to the Recruit tier. It is a separate
     * unit from Hive skill ELO.
     */
    public function getLevel(): int
    {
        return $this->player['level'];
    }

    /** Used for increasing level */
    public function getXp(): int
    {
        return $this->player['xp'];
    }

    public function getBadges(): array
    {
        return $this->player['badges'];
    }

    /** 
     * - Alien skill:  ( $skill - $offset )
     * - Marine skill: ( $skill + $offset )
     * 
     * @return int The players community server skill 
     */
    public function getSkill(string $type = 'average'): int
    {
        $skill  = $this->player['skill'];
        $offset = $this->player['skill_offset'];

        switch($type)
        {
            default:
            case 'average':
                return $skill;
            break;
            case 'alien':
                return ( $skill - $offset );
            break;
            case 'marine':
                return ( $skill + $offset );
            break;
        }
    }

    /** @return integer The players community server skill offset */
    public function getSkillOffset(): int
    {
        return $this->player['skill_offset'];
    }

    /** @return int The players community server Commander skill */
    public function getCommSkill(string $type = 'average'): int
    {
        $skill  = $this->player['comm_skill'];
        $offset = $this->player['comm_skill_offset'];

        switch($type)
        {
            default:
            case 'average':
                return $skill;
            break;
            case 'alien':
                return ( $skill - $offset );
            break;
            case 'marine':
                return ( $skill + $offset );
            break;
        }
    }

    /** @return int The players community server Commander skill offset */
    public function getCommSkillOffset(): int
    {
        return $this->player['comm_skill_offset'];
    }

    /**
     * This is the Hive's 'certainty' factor in a players
     * skill.
     *  
     * @return int The players community server adagrad 
     */
    public function getAdagrad(): int
    {
        return $this->player['adagrad_sum'];
    }

    /** @return int The players community server Commander adagrad */
    public function getCommAdagrad(): int
    {
        return $this->player['comm_adagrad_sum'];
    }

    /** 
     * The results of this can be dubious due
     * to a known bug where NS2 was doubling playtime
     * under certain conditions. The bugged results
     * were never corrected.
     */
    public function getPlaytime(string $type = 'total'): int
    {
        switch($type)
        {
            default:
            case 'total':
                return $this->player['time_played'];
            break;
            case 'alien':
                return $this->player['alien_playtime'];
            break;
            case 'marine':
                return $this->player['marine_playtime'];
            break;
            case 'comm':
            case 'commander':
                return $this->player['commander_time'];
            break;
        }
    }

    /** @return int The players matchmaking skill */
    public function getTdSkill(string $type = 'average'): int
    {
        $skill  = $this->player['td_skill'];
        $offset = $this->player['td_skill_offset'];

        switch($type)
        {
            default:
            case 'average':
                return $skill;
            break;
            case 'alien':
                return ( $skill - $offset );
            break;
            case 'marine':
                return ( $skill + $offset );
            break;
        }
    }

    /** @return int The players matchmaking skill offset */
    public function getTdSkillOffset(): int
    {
        return $this->player['td_skill_offset'];
    }

    /** @return int The players matchmaking Commander skill */
    public function getTdCommSkill(string $type = 'average'): int
    {
        $skill  = $this->player['td_comm_skill'];
        $offset = $this->player['td_comm_skill_offset'];

        switch($type)
        {
            default:
            case 'average':
                return $skill;
            break;
            case 'alien':
                return ( $skill - $offset );
            break;
            case 'marine':
                return ( $skill + $offset );
            break;
        }
    }

    /** @return int The players matchmaking Commander skill offset */
    public function getTdCommSkillOffset(): int
    {
        return $this->player['td_comm_skill_offset'];
    }

    /** @return int The players matchmaking adagrad */
    public function getTdAdagrad(): int
    {
        return $this->player['td_adagrad_sum'];
    }

    /** @return int The players matchmaking Commander adagrad */
    public function getTdCommAdagrad(): int
    {
        return $this->player['td_comm_adagrad_sum'];
    }

    public function getReinforcedTier(): int
    {
        return $this->player['reinforced_tier'];
    }

    public function getTdsStatus(): string
    {
        return $this->player['tds_status'];
    }
}