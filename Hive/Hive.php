<?php 

/**
 *  ▄▄▄·  ▄▄▄· ▄▄▄· ▄▄▄▄▄ ▄ .▄ ▄· ▄▌
 * ▐█ ▀█ ▐█ ▄█▐█ ▀█ •██  ██▪▐█▐█▪██▌
 * ▄█▀▀█  ██▀·▄█▀▀█  ▐█.▪██▀▐█▐█▌▐█▪
 * ▐█ ▪▐▌▐█▪·•▐█ ▪▐▌ ▐█▌·██▌▐▀ ▐█▀·.
 *  ▀  ▀ .▀    ▀  ▀  ▀▀▀ ▀▀▀ ·  ▀ •
 *  <https://fortreeforums.xyz/>
 *  Licensed under GPL-3.0-or-later 2021
 *
 *  This file is part of NS2 Hive PHP ("Hive").
 *
 *  Hive is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  Hive is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with Hive.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace apathy\Hive;

use apathy\Hive\Player;
use GuzzleHttp;

class Hive
{
    /** @var Player */
    public $player;

    protected $autoLoader;

    /** The NS2 Hive API */
    protected $endpoint = 'http://hive2.ns2cdt.com/api/get/playerData/';

    public function __construct(int $steamId)
    {
        $this->autoLoader = $this->registerAutoloader();

        $player = $this->getPlayer($steamId);

        if($player === NULL)
        {
            return print_r('Could not locate player in the Hive!');
        }

        $this->player = new Player($player);
    }

    public function getPlayer(int $steamId)
    {
        try
        {
            $player = $this->fetch($steamId);
            $player = GuzzleHttp\Utils::jsonDecode($player->getBody(), true);
        }
        catch(GuzzleHttp\Exception\GuzzleException $e)
        {
            return print_r($e->getMessage());
        }

        if(!isset($player['pid']) && !isset($player['steamid'])
        && !isset($player['alias']))
        {
            return NULL;
        }

        return $player;
    }

    protected function fetch(int $steamId, array $headers = [])
    {
        if(!empty($headers))
        {
            $requestHeaders = [
                'headers' => $headers
            ];
        }

        $url = $this->endpoint . $steamId;

        $client = new GuzzleHttp\Client();

        return $client->request('GET', $url, ( $requestHeaders ?? [] ));
    }

    protected function registerAutoloader()
    {
        /** @var \Composer\Autoload\ClassLoader $autoLoader */
        $autoLoader = require(dirname(dirname(__FILE__)) . '/vendor/autoload.php');
        $autoLoader->register();
        
        return $autoLoader;
    }
}